<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Avatar upload</title>
    <link rel="stylesheet" type="text/css" media="screen" href="css/styles.css" />
    <link href="https://fonts.googleapis.com/css?family=VT323&amp;subset=latin-ext" rel="stylesheet">
</head>
<body>
<?php
    $name = $lastname = $radio = "";
    $nameErr = $lastnameErr = $radioErr = "";
    
    if (isset($_GET['name'])) { $name = $_GET['name']; }
    if (isset($_GET['lastname'])) { $lastname = $_GET['lastname']; }
    if (isset($_GET['radio'])) { $radio = $_GET['radio']; }

    if (isset($_GET['nameErr'])) { $nameErr = $_GET['nameErr']; }
    if (isset($_GET['lastnameErr'])) { $lastnameErr = $_GET['lastnameErr']; }
    if (isset($_GET['radioErr'])) { $radioErr = $_GET['radioErr']; } 
?>
    <h1>Uploaduj svoj avatar</h1>
    <form method="post" name="upload" action="myPhoto.php" enctype="multipart/form-data" >
        <label for="name">Ime : </label><br>
        <input type="text" id="name" name="name" value="<?php echo $name;?>"><br>
        <span class="error">* <?php echo $nameErr;?></span>
        <br><br>

        <label for="lastname">Prezime: </label><br>
        <input type="text" id="lastname" name="lastname" value="<?php echo $lastname;?>"><br>
        <span class="error">* <?php echo $lastnameErr;?></span>
        <br><br>

        <label for= "radio"> Vidljivost: </label>
        <input type="radio" name="radio" id="radio" <?php if (isset($radio) && $radio=="public") echo "checked";?> value="public">Javno
        <input type="radio" name="radio" id="radio" <?php if (isset($radio) && $radio=="private") echo "checked";?> value="private">Privatno
        <span class="error">* <?php echo $radioErr;?></span>
        <br><br>

        <label for="if">File:</label><br><br>
        <input type="file" name="file" id="if"><br /><br />
        <input type="submit" name="sb" id="sb" value="Upload" />
    
    </form> 
</body>
</html>