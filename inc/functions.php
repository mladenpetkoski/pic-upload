<?php
function checkImage($file){
  
  if(exif_imagetype($file["tmp_name"]) == 2 || exif_imagetype($file["tmp_name"]) == 1){
    
    if($file["size"] < 8*1024){
      
      return true;
      
    }
    else{
      
      return false;
      
    }
    
  }
  else{
    
    return false;
    
  }
  
}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
?>