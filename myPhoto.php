<?php

    include "inc/functions.php";
    $name = $lastname = $radio = "";
    $nameErr = $lastnameErr = $radioErr = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST["name"])) {
    $nameErr = "Ime je neophodno";
  } 
  else {
    $name = test_input($_POST["name"]);
    if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
      $nameErr = "Samo su slova i razmak dozvoljeni"; 
    }
    else {
      $name1 = $_POST['name'];
      if (strlen($name1) <2) {    
        $nameErr = "Ime mora da sadrzi najmanje 2 slova";
      }
    }
  }

  if (empty($_POST["lastname"])) {
    $lastnameErr = "Prezime je neophodno";
  } 
  else {
    $lastname = test_input($_POST["lastname"]);
    if (!preg_match("/^[a-zA-Z ]*$/",$lastname)) {
      $lastnameErr = "samo su slova i razmak dozvoljeni"; 
    }
    else {
      $lastname1 = $_POST['lastname'];
      if (strlen($lastname1) <2) {    
        $lastnameErr = "Prezime mora da sadrzi najmanje 2 slova";
      }
    }
  }

  if (empty($_POST["radio"])) {
    $radioErr = "izbor je obavezan";
  } else {
    $radio = test_input($_POST["radio"]);
  }
  
  if (!empty($nameErr) or !empty($lastnameErr) or !empty($radioErr)) {
    $params = "name=" . urlencode($_POST["name"]);
    $params .= "&lastname=" . urlencode($_POST["lastname"]);
    $params .= "&radio=" . urlencode($_POST["radio"]);

    $params .= "&nameErr=" . urlencode($nameErr);
    $params .= "&lastnameErr=" . urlencode($lastnameErr);
    $params .= "&radioErr=" . urlencode($radioErr);
    
    header("Location: index.php?" . $params);
  } else {
    echo "<a href=\"index.php\">Vrati se na upload!</a>";
  }
}

if (checkImage ($_FILES["file"])){
  if(isset($_FILES["file"]) AND is_uploaded_file($_FILES['file']['tmp_name'])) {
    $file_name = $_FILES['file']["name"];
    $file_temp = $_FILES["file"]["tmp_name"];
    $file_size = $_FILES["file"]["size"];
    $file_type = $_FILES["file"]["type"];
    $file_error = $_FILES['file']["error"];
    if ($file_error >0) {
        echo "Doslo je do greske";
    }
    else {
        echo exif_imagetype($file_temp)."<br />";
        if (!exif_imagetype($file_temp)) {
          exit("Fajl nije slika!");
        }
        /*elseif (exif_imagetype($file_temp) !=2 && exif_imagetype($file_temp) !=3){
          exit("Slika mora biti u jpeg ili png formatu");
        }*/
        echo "Ime poslate datoteke <br />";
        echo "Privremena lokacija datoteke<br />";
        echo "Veličina poslate datoteke u bajtovima <br />";
        echo "Tip poslate datoteke <br />";
        echo "Kod greške $file_error <br />";
        $ext_temp = explode(".", $file_name);
        $extension = end($ext_temp);
        
        
        $file_name_name = strtolower ($_POST['name'])."_".strtolower ($_POST['lastname']);
        $new_file_name = str_replace(" ","_", $file_name_name). "-" .date("YmdHis") . "-".rand(1, 10). ".$extension";
        
        
        $folder = $_POST['radio'];
        $upload = "$folder/$new_file_name";
        if (!is_dir($folder)) {
          mkdir($folder);
        }
        if (!file_exists($upload)) {
            if (move_uploaded_file($file_temp, $upload)) {
                $size = getimagesize($upload);
                foreach ($size as $key => $value)
                echo "$key = $value<br />";
                echo "<img src=\"$upload\" $size[3] border=\"0\" alt=\"$file_name\" />";
            }
            else {
              echo "<p><b>Greska!</b></p>";
            }
        }
        else {
            for($i=1; $i<9; $i++){
                if(!file_exists($directory.'/'.$file_name_name.$i.'.'.$extension)){
                  move_uploaded_file($file_temp, $folder.'/'.$file_name_name.$i.'.'.$extension);
                  break;
                }
                else{
                  echo "<p><b>Fajl sa ovim imenom vec postoji!</b></p>";
                }
            }
        }
    }
}
}

else{
  echo "file is not supported";

}

?>
</body>
</html>



